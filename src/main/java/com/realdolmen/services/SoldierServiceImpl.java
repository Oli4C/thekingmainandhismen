package com.realdolmen.services;

import com.realdolmen.domain.Soldier;
import com.realdolmen.repository.BarackRepository;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class SoldierServiceImpl implements SoldierCommands{

    private BarackRepository barackRepository = new BarackRepository();

    @Override
    public void sendToBarracks(Soldier soldier) {
        log.info("A soldier is being added to barrack");
        barackRepository.save(soldier);
    }

    @Override
    public List<Soldier> findAll() {
        return barackRepository.findAllFromDb();
    }
}
