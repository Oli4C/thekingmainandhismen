package com.realdolmen.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@ToString(callSuper = true)
@SuperBuilder(builderMethodName = "soldierBuilder")
public class Soldier extends Human {
    public static final String MY_KING = "King Main";
    private int defence;
    private int attack;
    private String weapon;
    private int recoverySpeed;
    private int health;

    public static SoldierBuilder builder(String name) {
        return soldierBuilder().name(name).attack(60).defence(50).weapon("SWORD").recoverySpeed(20).health(100);
    }
}
